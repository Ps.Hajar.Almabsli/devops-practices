FROM openjdk:8
ENV database_url="jdbc:mysql://db:3306/db?allowPublicKeyRetrieval=true"
ENV username="db"
ENV password="P@ssw0rd"
ENV port="8090"
ENV profile="mysql"
COPY ./target/assignment*.jar /usr/bin/
WORKDIR /usr/bin/
RUN chmod +x /usr/bin/assignment*.jar
EXPOSE 8090
ENTRYPOINT java -jar -Dserver.port=$port -Dspring.profiles.active=$profile -Dspring.datasource.username=$username -Dspring.datasource.password=$password -Dspring.datasource.url=$database_url assignment*.jar
